<?php

namespace Drupal\condition_plugins_commerce\Plugin;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Condition\ConditionPluginBase;

/**
 * Provides the base class for conditions.
 */
abstract class ConditionBase extends ConditionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $cache_contexts = Cache::mergeTags(parent::getCacheContexts(), ['route.condition_plugins_commerce_order']);

    return Cache::mergeContexts($cache_contexts, $this->getOrder()->getCacheContexts());
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return Cache::mergeTags(parent::getCacheTags(), $this->getOrder()->getCacheTags());
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return Cache::mergeMaxAges(parent::getCacheMaxAge(), $this->getOrder()->getCacheMaxAge());
  }

  /**
   * Gets the comparison operators.
   *
   * @return array
   *   The comparison operators.
   */
  protected function getComparisonOperators() {
    return [
      '>' => $this->t('Greater than'),
      '>=' => $this->t('Greater than or equal to'),
      '<=' => $this->t('Less than or equal to'),
      '<' => $this->t('Less than'),
      '==' => $this->t('Equals'),
    ];
  }

  /**
   * Returns the commerce order from context.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface|null
   *   The commerce order.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function getOrder() {
    return $this->getContextValue('commerce_order');
  }

}
