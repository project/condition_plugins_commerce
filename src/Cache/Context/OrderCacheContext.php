<?php

namespace Drupal\condition_plugins_commerce\Cache\Context;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Defines the OrderCacheContext service, for "per commerce order" caching.
 *
 * Cache context ID: 'route.condition_plugins_commerce.order'.
 */
class OrderCacheContext implements CacheContextInterface, ContainerAwareInterface {

  use ContainerAwareTrait;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a new OrderCacheContext class.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(RequestStack $request_stack) {
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return t('Commerce Order');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext() {
    if ($commerce_order = $this->requestStack->getCurrentRequest()->get('commerce_order')) {
      return $commerce_order->id();
    }

    return 'none';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata() {
    return new CacheableMetadata();
  }

}
