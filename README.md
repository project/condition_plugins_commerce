### SUMMARY
This module adds a Condition plugin collection for commerce and provides an order as context on order routes.

### INSTALLATION
Using composer:
```
composer require drupal/condition_plugins_commerce --sort-packages
```
No configuration required.

### DRUPAL CORE PLUGIN CONDITION LIST
- **Commerce order bundle**: Provides an order type condition.
- **Order has base field value**: Provides the order has field value condition.
- **Order has product variation**: Provides the order has product variation condition.
- **Order has product variation with base field value**: Provides the order has a product variation with field value condition.

### DRUPAL COMMERCE PLUGIN CONDITION LIST
- **Order has base field value**: Provides the order has field value condition.
- **Order has product variation**: Provides the order has product variation condition.
- **Order has product variation with base field value**: Provides the order has a product variation with field value condition.

### SPONSORS
- [Fundación UNICEF Comité Español](https://www.unicef.es)

### CONTACT
Developed and maintained by Cambrico (http://cambrico.net).

Get in touch with us for customizations and consultancy:
http://cambrico.net/contact

#### Current maintainers:
- Pedro Cambra [(pcambra)](https://www.drupal.org/u/pcambra)
- Manuel Egío [(facine)](https://www.drupal.org/u/facine)
